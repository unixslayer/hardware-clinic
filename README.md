# Hardware Clinic

Simple Symfony 4 App for Managing a Hardware Clinic.

You can use following docker-compose configuration to run this application:

```yaml
version: '3.3'

services:
  app:
    image: unixslayer/hardware-clinic
    ports:
      - 80:80
    environment:
      SERVER_NAME: hardware-clinic.local
      DATABASE_HOST: database
      DATABASE_PORT: 3306
      DATABASE_USER: clinic
      DATABASE_NAME: clinic
      DATABASE_PASS: hardwareclinic

  database:
    image: percona:5.7
    environment:
      MYSQL_ROOT_PASSWORD: clinicroot
      MYSQL_DATABASE: clinic
      MYSQL_USER: clinic
      MYSQL_PASSWORD: hardwareclinic

```

After (re)start, app container will perform database migrations and in addition, dictionaries installation based on prepared fixture.

You can also load demo data:

    $ docker/cli demo.sh

Demo data is based on Doctrine fixtures and contains:

- 20 randomly generated customer users with login/password schema: customer1/customer
- 20 randomly generated employee users with login/password schema: employee1/employee
- ~40 mobile devices
- 1000 orders randomly assigned with devices, customers and employees

Add defined server name to your local /etc/hosts:

```sh
127.0.0.1   hardware-clinic.local
```

If you visit http://hardware-clinic.local, you should see login form.
