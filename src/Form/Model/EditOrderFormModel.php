<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class EditOrderFormModel
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please choose status")
     * @Assert\Choice(callback={"App\Domain\OrderStatusType", "getStatuses"})
     */
    public $status;

    /**
     * @var string
     */
    public $assignee;
}
