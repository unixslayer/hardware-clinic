<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class CreateOrderFormModel
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please choose order type")
     */
    public $type;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please choose device type")
     */
    public $device;

    /**
     * @var string
     */
    public $description;
}
