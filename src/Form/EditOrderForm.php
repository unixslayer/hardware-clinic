<?php

namespace App\Form;

use App\Domain\OrderStatusType;
use App\Entity\User;
use App\Form\Model\EditOrderFormModel;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditOrderForm extends AbstractType
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, ['choices' => $this->getStatusChoices(), 'translation_domain' => 'order'])
            ->add('assignee', ChoiceType::class, ['choices' => $this->getAssigneeChoices()])
            ->add('save', SubmitType::class, ['label' => 'Submit']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EditOrderFormModel::class,
            'translation_domain' => 'order',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return null;
    }

    /**
     * @return array|string[]
     */
    private function getStatusChoices()
    {
        $choices = OrderStatusType::getStatuses();

        return array_combine($choices, $choices);
    }

    /**
     * @return array
     */
    private function getAssigneeChoices()
    {
        $employees = $this->userRepository->getEmployees();

        $initial = ['' => ''];
        return array_flip(array_reduce($employees, function (array $carrier, User $employee) {
            $carrier[$employee->getId()->toString()] = $employee->getFullName();

            return $carrier;
        }, $initial));
    }
}
