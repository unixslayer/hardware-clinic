<?php

namespace App\Form;

use App\DBAL\DeviceType;
use App\Domain\OrderType;
use App\Entity\Device;
use App\Form\Model\CreateOrderFormModel;
use App\Repository\DeviceRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateOrderForm extends AbstractType
{
    /**
     * @var DeviceRepository
     */
    private $deviceRepository;

    /**
     * @param DeviceRepository $deviceRepository
     */
    public function __construct(DeviceRepository $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, ['choices' => $this->getTypeChoices()])
            ->add('device', ChoiceType::class, ['choices' => $this->getDeviceChoices()])
            ->add('description', TextareaType::class, ['attr' => ['rows' => 10]])
            ->add('save', SubmitType::class, ['label' => 'Submit']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreateOrderFormModel::class,
            'translation_domain' => 'order',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return null;
    }

    /**
     * @return array
     */
    private function getDeviceChoices(): array
    {
        $devices = $this->deviceRepository->findBy([], ['brand' => 'ASC', 'year' => 'ASC']);

        return array_flip(array_reduce($devices, function (array $carrier, Device $device) {
            $carrier[$device->getId()->toString()] = sprintf('%s %s (%d)', $device->getBrand(), $device->getModel(), $device->getYear());

            return $carrier;
        }, []));
    }

    /**
     * @return array
     */
    private function getTypeChoices(): array
    {
        $orderTypes = OrderType::getTypes();

        return array_combine($orderTypes, $orderTypes);
    }
}
