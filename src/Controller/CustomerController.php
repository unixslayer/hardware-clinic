<?php

namespace App\Controller;

use App\Domain\Command\CreateOrderCommand;
use App\Form\CreateOrderForm;
use App\Form\Model\CreateOrderFormModel;
use App\Repository\OrderRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PropertyAccess\Exception\InvalidPropertyPathException;
use Symfony\Component\Routing\Annotation\Route;

class CustomerController extends AbstractController
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    /**
     * @param OrderRepository     $orderRepository
     * @param MessageBusInterface $messageBus
     */
    public function __construct(OrderRepository $orderRepository, MessageBusInterface $messageBus)
    {
        $this->orderRepository = $orderRepository;
        $this->messageBus = $messageBus;
    }

    /**
     * @return Response
     *
     * @Route("/customer", name="customer_index")
     */
    public function index()
    {
        return $this->render('customer/index.html.twig');
    }

    /**
     * @return Response
     *
     * @Route("/customer/orders", name="customer_orders")
     */
    public function orders()
    {
        $orders = $this->orderRepository->findBy(['reporter' => $this->getUser()], ['createdAt' => 'DESC']);

        return $this->render('customer/orders.html.twig', ['orders' => $orders]);
    }

    /**
     * @return Response
     *
     * @Route("/customer/order/{id}", name="customer_orders_view")
     */
    public function ordersView(string $id)
    {
        $uuid = Uuid::fromString($id);
        $order = $this->orderRepository->findOneBy(['reporter' => $this->getUser(), 'id' => $uuid]);

        return $this->render('customer/orders.view.html.twig', ['order' => $order]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/customer/orders/create", name="customer_orders_create")
     */
    public function createOrder(Request $request)
    {
        $model = new CreateOrderFormModel();
        $form = $this->createForm(CreateOrderForm::class, $model);
        $form->handleRequest($request);

        try {
            if ($form->isSubmitted() && $form->isValid()) {
                /** @var CreateOrderFormModel $data */
                $data = $form->getData();

                $deviceId = Uuid::fromString($data->device);
                $reporter = $this->getUser();

                $command = new CreateOrderCommand($deviceId, $data->type, $reporter, $data->description);
                $this->messageBus->dispatch($command);

                return $this->redirectToRoute('customer_orders');
            }
        } catch (InvalidPropertyPathException $exception) {
            dump($exception);
            die;
        } catch (\Throwable $exception) {
            dump($exception);
            die;
        }

        return $this->render('customer/orders.create.html.twig', ['form' => $form->createView()]);
    }
}
