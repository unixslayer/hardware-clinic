<?php

namespace App\Controller;

use App\Domain\Command\UpdateOrderCommand;
use App\Entity\Order;
use App\Form\EditOrderForm;
use App\Form\Model\EditOrderFormModel;
use App\Infrastructure\Exception\OrderNotExistsException;
use App\Infrastructure\Report\CompositeOrderReportInterface;
use App\Infrastructure\Report\OrderReportInterface;
use App\Repository\OrderRepository;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class EmployeeController extends AbstractController
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    /**
     * @var CompositeOrderReportInterface
     */
    private $report;

    /**
     * @param OrderRepository               $orderRepository
     * @param MessageBusInterface           $messageBus
     * @param CompositeOrderReportInterface $report
     */
    public function __construct(
        OrderRepository $orderRepository,
        MessageBusInterface $messageBus,
        CompositeOrderReportInterface $report
    ) {
        $this->orderRepository = $orderRepository;
        $this->messageBus = $messageBus;
        $this->report = $report;
    }

    /**
     * @return Response
     *
     * @Route("/employee", name="employee_index")
     */
    public function index()
    {
        $orders = $this->orderRepository->findAll();

        return $this->render('employee/index.html.twig', ['orders' => $orders]);
    }

    /**
     * @return Response
     *
     * @Route("/employee/orders", name="employee_all_orders")
     */
    public function orders()
    {
        $orders = $this->orderRepository->findBy([], ['createdAt' => 'DESC']);

        return $this->render('employee/orders.html.twig', ['orders' => $orders, 'headTitle' => 'Orders!']);
    }

    /**
     * @return Response
     *
     * @Route("/employee/orders/my", name="employee_my_orders")
     */
    public function myOrders()
    {
        $orders = $this->orderRepository->findBy(['assignee' => $this->getUser()], ['createdAt' => 'DESC']);

        return $this->render('employee/orders.html.twig', ['orders' => $orders, 'headTitle' => 'My Orders!']);
    }

    /**
     * @param string $report
     *
     * @return Response
     *
     * @Route("/employee/reports/{report}", name="employee_report", requirements={"report"="\w+"})
     */
    public function report(string $report = 'order_by_type')
    {
        $pivot = $this->report->getReportPivot($report);

        return $this->render('employee/report.html.twig', ['pivot' => $pivot]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/employee/order/{id}", name="employee_orders_edit", methods={"GET", "POST"})
     */
    public function editOrder(Request $request, string $id)
    {
        $uuid = Uuid::fromString($id);
        $model = $this->getFormModel($uuid);
        $form = $this->createForm(EditOrderForm::class, $model);
        $form->handleRequest($request);
        try {
            if ($form->isSubmitted() && $form->isValid()) {
                /** @var EditOrderFormModel $data */
                $data = $form->getData();

                $assigneeId = empty($data->assignee) ? null : Uuid::fromString($data->assignee);

                $command = new UpdateOrderCommand($uuid, $data->status, $assigneeId);
                $this->messageBus->dispatch($command);
                $this->addFlash('info', 'order_saved');

                return $this->redirectToRoute('employee_orders_edit', ['id' => $id]);
            }
        } catch (\Throwable $exception) {
            $form->addError(new FormError($exception->getMessage()));
        }

        $order = $this->orderRepository->findOneBy(['id' => Uuid::fromString($id)]);

        return $this->render('employee/edit.html.twig', ['form' => $form->createView(), 'order' => $order]);
    }

    /**
     * @param UuidInterface $id
     *
     * @return EditOrderFormModel
     * @throws OrderNotExistsException
     */
    private function getFormModel(UuidInterface $id): EditOrderFormModel
    {
        $order = $this->orderRepository->find($id);

        if (!$order instanceof Order) {
            throw new OrderNotExistsException($id);
        }

        $model = new EditOrderFormModel();
        $model->status = $order->getStatus();
        $model->assignee = $order->hasAssignee() ? $order->getAssignee()->getId() : '';

        return $model;
    }
}
