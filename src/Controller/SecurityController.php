<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\OnAuthenticationSuccessRedirectProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @var OnAuthenticationSuccessRedirectProvider
     */
    private $successRedirectProvider;

    /**
     * @param OnAuthenticationSuccessRedirectProvider $successRedirectProvider
     */
    public function __construct(OnAuthenticationSuccessRedirectProvider $successRedirectProvider)
    {
        $this->successRedirectProvider = $successRedirectProvider;
    }

    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser() instanceof User) {
            return $this->successRedirectProvider->getRedirect();
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
}
