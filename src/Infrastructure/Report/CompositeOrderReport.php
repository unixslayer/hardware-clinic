<?php

namespace App\Infrastructure\Report;

use App\Infrastructure\Exception\OrderReportNotExistsException;

class CompositeOrderReport implements CompositeOrderReportInterface
{
    /**
     * @var OrderReportInterface[]
     */
    private $reports = [];

    /**
     * @param string               $name
     * @param OrderReportInterface $report
     */
    public function addReport(string $name, OrderReportInterface $report): void
    {
        $this->reports[$name] = $report;
    }

    /**
     * @param string $report
     *
     * @return array
     * @throws OrderReportNotExistsException
     */
    public function getReportPivot(string $report): array
    {
        if (array_key_exists($report, $this->reports)) {
            return $this->reports[$report]->getPivot();
        }

        throw new OrderReportNotExistsException($report);
    }
}