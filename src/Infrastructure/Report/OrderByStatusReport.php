<?php

namespace App\Infrastructure\Report;

class OrderByStatusReport extends AbstractOrderReport
{
    /**
     * {@inheritdoc}
     */
    protected function getSelectPart(): string
    {
        return 'SELECT o.status';
    }

    /**
     * {@inheritdoc}
     */
    protected function getGroupPart(): string
    {
        return ' GROUP BY o.status';
    }

    /**
     * {@inheritdoc}
     */
    protected function getJoinPart(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    protected function getOrderByPart(): string
    {
        return ' ORDER BY o.status';
    }
}