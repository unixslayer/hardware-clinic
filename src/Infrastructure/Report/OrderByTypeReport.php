<?php

namespace App\Infrastructure\Report;

class OrderByTypeReport extends AbstractOrderReport
{
    /**
     * {@inheritdoc}
     */
    protected function getSelectPart(): string
    {
        return 'SELECT d.type';
    }

    /**
     * {@inheritdoc}
     */
    protected function getGroupPart(): string
    {
        return ' GROUP BY d.type';
    }

    /**
     * {@inheritdoc}
     */
    protected function getJoinPart(): string
    {
        return ' INNER JOIN devices d ON d.id = o.device_id';
    }

    /**
     * {@inheritdoc}
     */
    protected function getOrderByPart(): string
    {
        return ' ORDER BY d.type';
    }
}