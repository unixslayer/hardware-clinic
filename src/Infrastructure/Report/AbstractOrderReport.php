<?php

namespace App\Infrastructure\Report;

use App\Domain\OrderType;
use Doctrine\DBAL\Connection;

abstract class AbstractOrderReport implements OrderReportInterface
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function getPivot(): array
    {
        $sql = $this->getSelectPart();

        foreach (OrderType::getTypes() as $type) {
            $sql .= sprintf(', COUNT(CASE WHEN o.type = \'%s\' THEN 1 ELSE NULL END) AS \'%s\'', $type, $type);
        }

        $sql .= ' FROM orders o';

        $sql .= $this->getJoinPart();
        $sql .= $this->getGroupPart();
        $sql .= $this->getOrderByPart();

        return $this->connection->executeQuery($sql)->fetchAll();
    }

    /**
     * @return string
     */
    abstract protected function getSelectPart(): string;

    /**
     * @return string
     */
    abstract protected function getGroupPart(): string;

    /**
     * @return string
     */
    abstract protected function getJoinPart(): string;

    /**
     * @return string
     */
    abstract protected function getOrderByPart(): string;
}