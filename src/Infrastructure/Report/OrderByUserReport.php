<?php

namespace App\Infrastructure\Report;

class OrderByUserReport extends AbstractOrderReport
{
    /**
     * {@inheritdoc}
     */
    protected function getSelectPart(): string
    {
        return 'SELECT CONCAT(u.firstname, \' \', u.lastname) AS assignee';
    }

    /**
     * {@inheritdoc}
     */
    protected function getGroupPart(): string
    {
        return ' GROUP BY u.username';
    }

    /**
     * {@inheritdoc}
     */
    protected function getJoinPart(): string
    {
        return ' INNER JOIN users u ON u.id = o.assignee_id';
    }

    /**
     * {@inheritdoc}
     */
    protected function getOrderByPart(): string
    {
        return ' ORDER BY u.id';
    }
}