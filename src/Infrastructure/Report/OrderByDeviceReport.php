<?php

namespace App\Infrastructure\Report;

class OrderByDeviceReport extends AbstractOrderReport
{
    /**
     * {@inheritdoc}
     */
    protected function getSelectPart(): string
    {
        return 'SELECT CONCAT(d.brand, \' \', d.model, \' (\', d.year, \')\') AS device';
    }

    /**
     * {@inheritdoc}
     */
    protected function getGroupPart(): string
    {
        return ' GROUP BY d.id';
    }

    /**
     * {@inheritdoc}
     */
    protected function getJoinPart(): string
    {
        return ' INNER JOIN devices d ON d.id = o.device_id';
    }

    /**
     * {@inheritdoc}
     */
    protected function getOrderByPart(): string
    {
        return ' ORDER BY d.brand ASC, d.year ASC';
    }
}