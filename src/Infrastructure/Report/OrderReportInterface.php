<?php

namespace App\Infrastructure\Report;

interface OrderReportInterface
{
    /**
     * @return array
     */
    public function getPivot(): array;
}
