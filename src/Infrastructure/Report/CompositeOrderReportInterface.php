<?php

namespace App\Infrastructure\Report;

interface CompositeOrderReportInterface
{
    /**
     * @param string $report
     *
     * @return array
     */
    public function getReportPivot(string $report): array;
}