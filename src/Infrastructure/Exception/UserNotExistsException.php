<?php

namespace App\Infrastructure\Exception;

use Ramsey\Uuid\UuidInterface;

class UserNotExistsException extends \Exception
{
    /**
     * @param UuidInterface   $id
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct(UuidInterface $id, int $code = 0, \Throwable $previous = null)
    {
        $message = sprintf("User by id '%s' not exists.", $id->toString());

        parent::__construct($message, $code, $previous);
    }
}