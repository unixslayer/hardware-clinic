<?php

namespace App\Infrastructure\Exception;

class OrderReportNotExistsException extends \Exception
{
    /**
     * @param string          $reportName
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $reportName, int $code = 0, \Throwable $previous = null)
    {
        $message = sprintf("Requested report '%s' not exists.", $reportName);

        parent::__construct($message, $code, $previous);
    }
}