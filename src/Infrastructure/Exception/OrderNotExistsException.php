<?php

namespace App\Infrastructure\Exception;

use Ramsey\Uuid\UuidInterface;
use Throwable;

class OrderNotExistsException extends \Exception
{
    /**
     * @param UuidInterface  $id
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(UuidInterface $id, int $code = 0, Throwable $previous = null)
    {
        $message = sprintf("Order by id '%s' not exists.", $id->toString());

        parent::__construct($message, $code, $previous);
    }

}