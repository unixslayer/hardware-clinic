<?php

namespace App\Infrastructure\Handler;

use App\Domain\Command\UpdateOrderCommand;
use App\Entity\Order;
use App\Entity\User;
use App\Infrastructure\Exception\OrderNotExistsException;
use App\Infrastructure\Exception\UserNotExistsException;
use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdateOrderCommandHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param OrderRepository        $orderRepository
     * @param UserRepository         $userRepository
     * @param EntityManagerInterface $manager
     */
    public function __construct(
        OrderRepository $orderRepository,
        UserRepository $userRepository,
        EntityManagerInterface $manager
    ) {
        $this->manager = $manager;
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }

    public function __invoke(UpdateOrderCommand $command)
    {
        $this->manager->beginTransaction();

        try {
            $order = $this->getOrder($command->getOrderId());

            $order->setAssignee($command->hasAssigneeId() ? $this->getAssignee($command->getAssigneeId()) : null);
            $order->setStatus($command->getStatus());

            $this->manager->persist($order);
            $this->manager->flush();
            $this->manager->commit();
        } catch (\Throwable $exception) {
            $this->manager->rollback();

            throw $exception;
        }
    }

    /**
     * @param UuidInterface $uuid
     *
     * @return Order
     * @throws OrderNotExistsException
     */
    private function getOrder(UuidInterface $uuid): Order
    {
        $order = $this->orderRepository->find($uuid->toString());

        if (!$order instanceof Order) {
            throw new OrderNotExistsException($uuid);
        }

        return $order;
    }

    /**
     * @param UuidInterface $assigneeId
     *
     * @return User
     * @throws UserNotExistsException
     */
    private function getAssignee(UuidInterface $assigneeId): User
    {
        $user = $this->userRepository->find($assigneeId->toString());

        if (!$user instanceof User) {
            throw new UserNotExistsException($assigneeId);
        }

        return $user;
    }
}
