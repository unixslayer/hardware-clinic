<?php

namespace App\Infrastructure\Handler;

use App\Domain\Command\CreateOrderCommand;
use App\Entity\Device;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Security;

class CreateOrderCommandHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param CreateOrderCommand $command
     *
     * @throws \Throwable
     */
    public function __invoke(CreateOrderCommand $command)
    {
        $this->manager->beginTransaction();

        try {
            $device = $this->getDevice($command->getDeviceId());
            $order = new Order($device, $command->getReporter(), $command->getType(), $command->getDescription());
            $this->manager->persist($order);
            $this->manager->flush();
            $this->manager->commit();
        } catch (\Throwable $exception) {
            $this->manager->rollback();

            throw $exception;
        }
    }

    /**
     * @param UuidInterface $deviceId
     *
     * @return Device
     */
    private function getDevice(UuidInterface $deviceId): Device
    {
        return $this->manager->getRepository(Device::class)->find($deviceId);
    }
}
