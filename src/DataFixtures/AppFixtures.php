<?php

namespace App\DataFixtures;

use App\Domain\DeviceType;
use App\Entity\Device;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $this->generateDevices($manager);
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['clinic'];
    }

    /**
     * @param ObjectManager $manager
     */
    private function generateDevices(ObjectManager $manager)
    {
        foreach ($this->getMobilesData() as $mobile) {
            $device = new Device(DeviceType::TYPE_MOBILE, $mobile['brand'], $mobile['model'], $mobile['year']);
            $manager->persist($device);
        }
    }

    /**
     * @return array
     */
    private function getMobilesData(): array
    {
        return [
            ['brand' => 'LG', 'model' => 'Optimus G', 'year' => 2013],
            ['brand' => 'LG', 'model' => 'Optimus G Pro', 'year' => 2013],
            ['brand' => 'LG', 'model' => 'G2', 'year' => 2013],
            ['brand' => 'LG', 'model' => 'G3', 'year' => 2014],
            ['brand' => 'LG', 'model' => 'G4', 'year' => 2015],
            ['brand' => 'LG', 'model' => 'G5', 'year' => 2016],
            ['brand' => 'LG', 'model' => 'G6', 'year' => 2017],
            ['brand' => 'LG', 'model' => 'G7', 'year' => 2018],
            ['brand' => 'LG', 'model' => 'G3 Stylus', 'year' => 2014],
            ['brand' => 'Huawei', 'model' => 'P8', 'year' => 2015],
            ['brand' => 'Huawei', 'model' => 'P9', 'year' => 2016],
            ['brand' => 'Huawei', 'model' => 'Mate 8', 'year' => 2015],
            ['brand' => 'Huawei', 'model' => 'Mate 9', 'year' => 2016],
            ['brand' => 'Huawei', 'model' => 'Mate 10', 'year' => 2017],
            ['brand' => 'Huawei', 'model' => 'Mate 10 Pro', 'year' => 2017],
            ['brand' => 'Huawei', 'model' => 'Mate 10 Lite', 'year' => 2017],
            ['brand' => 'Huawei', 'model' => 'Mate 20', 'year' => 2018],
            ['brand' => 'Huawei', 'model' => 'Mate 20 Pro', 'year' => 2018],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note', 'year' => 2011],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note 2', 'year' => 2012],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note 3', 'year' => 2013],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note 4', 'year' => 2014],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note Edge', 'year' => 2014],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note 5', 'year' => 2015],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note 7', 'year' => 2016],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note 8', 'year' => 2017],
            ['brand' => 'Samsung', 'model' => 'Galaxy Note 9', 'year' => 2018],
            ['brand' => 'Samsung', 'model' => 'Galaxy S', 'year' => 2010],
            ['brand' => 'Samsung', 'model' => 'Galaxy SL', 'year' => 2011],
            ['brand' => 'Samsung', 'model' => 'Galaxy S2', 'year' => 2011],
            ['brand' => 'Samsung', 'model' => 'Galaxy S Advance', 'year' => 2012],
            ['brand' => 'Samsung', 'model' => 'Galaxy S3', 'year' => 2012],
            ['brand' => 'Samsung', 'model' => 'Galaxy S4', 'year' => 2013],
            ['brand' => 'Samsung', 'model' => 'Galaxy S5', 'year' => 2014],
            ['brand' => 'Samsung', 'model' => 'Galaxy S6', 'year' => 2015],
            ['brand' => 'Samsung', 'model' => 'Galaxy S7', 'year' => 2016],
            ['brand' => 'Samsung', 'model' => 'Galaxy S8', 'year' => 2017],
            ['brand' => 'Samsung', 'model' => 'Galaxy S9', 'year' => 2018],
            ['brand' => 'Samsung', 'model' => 'Galaxy S10', 'year' => 2019],
        ];
    }
}
