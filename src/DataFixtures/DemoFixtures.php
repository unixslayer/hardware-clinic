<?php

namespace App\DataFixtures;

use App\Domain\OrderStatusType;
use App\Domain\OrderType;
use App\Entity\Device;
use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\Lorem;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DemoFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $employees = $this->loadUsers($manager, 'employee%d', 'employee', ['ROLE_EMPLOYEE']);
        $customers = $this->loadUsers($manager, 'customer%d', 'customer', ['ROLE_CUSTOMER']);

        $this->loadOrders($manager, $employees, $customers);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string        $usernameFormat
     * @param string        $password
     * @param array         $roles
     *
     * @return User[]
     */
    private function loadUsers(ObjectManager $manager, string $usernameFormat, string $password, array $roles): array
    {
        $faker = Factory::create();
        $users = [];
        for ($i = 1; $i <= 20; $i++) {
            $username = sprintf($usernameFormat, $i);
            $firstname = $faker->firstName;
            $lastname = $faker->lastName;

            $encoded = $this->encoder->encodePassword(new User($username, $password, $firstname, $lastname), $password);

            $user = new User($username, $encoded, $firstname, $lastname, $roles);
            $manager->persist($user);

            $users[] = $user;
        }

        return $users;
    }

    /**
     * @param ObjectManager $manager
     * @param User[]        $employees
     * @param User[]        $customers
     */
    private function loadOrders(ObjectManager $manager, array $employees, array $customers)
    {
        $devices = $manager->getRepository(Device::class)->findAll();
        $statuses = OrderStatusType::getStatuses();
        $types = OrderType::getTypes();

        for ($i = 1; $i <= 1000; $i++) {
            $device = $devices[random_int(0, count($devices) - 1)];
            $employee = $employees[random_int(0, count($employees) - 1)];
            $customer = $customers[random_int(0, count($customers) - 1)];
            $status = $statuses[random_int(0, count($statuses) - 1)];
            $type = $types[random_int(0, count($types) - 1)];

            $description = Lorem::paragraphs(random_int(3, 10), true);

            $order = new Order($device, $customer, $type, $description, $status, $employee);

            $manager->persist($order);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getGroups(): array
    {
        return ['demo'];
    }
}
