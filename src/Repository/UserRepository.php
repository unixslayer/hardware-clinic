<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getEmployees()
    {
        $query = $this->createQueryBuilder('u');
        $query->where("JSON_SEARCH(u.roles, 'one', :role) IS NOT NULL");
        $query->setParameter('role', 'ROLE_EMPLOYEE');
        $query->orderBy($query->expr()->asc('u.username'));

        return $query->getQuery()->execute();
    }
}
