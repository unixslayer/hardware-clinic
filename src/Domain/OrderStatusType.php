<?php

namespace App\Domain;

class OrderStatusType
{
    const NEW = 'new';
    const ACCEPTED = 'accepted';
    const IN_PROGRESS = 'in_progress';
    const READY = 'ready';
    const REJECTED = 'rejected';

    /**
     * @return string[]
     */
    public static function getStatuses(): array
    {
        return [self::NEW, self::ACCEPTED, self::IN_PROGRESS, self::READY, self::REJECTED];
    }
}
