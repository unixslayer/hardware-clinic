<?php

namespace App\Domain;

class OrderType
{
    const REPAIR_SCREEN = 'repair_screen';
    const REPLACE_HARD_DRIVE = 'replace_hard_drive';
    const REPLACE_BATTERY = 'replace_battery';
    const FORMAT_HARD_DRIVE = 'format_hard_drive';
    const REINSTALL_OS = 'reinstall_os';

    /**
     * @return string[]
     */
    public static function getTypes(): array
    {
        return [self::REPAIR_SCREEN, self::REPLACE_HARD_DRIVE, self::REPLACE_BATTERY, self::FORMAT_HARD_DRIVE, self::REINSTALL_OS];
    }
}
