<?php

namespace App\Domain\Command;

use App\Entity\User;
use Ramsey\Uuid\UuidInterface;

class CreateOrderCommand
{
    /**
     * @var UuidInterface
     */
    private $deviceId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $description;

    /**
     * @var User
     */
    private $reporter;

    /**
     * @param UuidInterface $deviceId
     * @param string        $type
     * @param User          $reporter
     * @param string        $description
     */
    public function __construct(
        UuidInterface $deviceId,
        string $type,
        User $reporter,
        string $description = null
    ) {
        $this->deviceId = $deviceId;
        $this->type = $type;
        $this->description = $description;
        $this->reporter = $reporter;
    }

    /**
     * @return UuidInterface
     */
    public function getDeviceId(): UuidInterface
    {
        return $this->deviceId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function hasDescription(): bool
    {
        return $this->description !== null;
    }

    /**
     * @return User
     */
    public function getReporter(): User
    {
        return $this->reporter;
    }
}