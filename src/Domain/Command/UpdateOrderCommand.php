<?php

namespace App\Domain\Command;

use Ramsey\Uuid\UuidInterface;

class UpdateOrderCommand
{
    /**
     * @var UuidInterface
     */
    private $orderId;

    /**
     * @var UuidInterface
     */
    private $assigneeId;

    /**
     * @var string
     */
    private $status;

    /**
     * @param UuidInterface      $orderId
     * @param string             $status
     * @param UuidInterface|null $assigneeId
     */
    public function __construct(UuidInterface $orderId, string $status, UuidInterface $assigneeId = null)
    {
        $this->orderId = $orderId;
        $this->assigneeId = $assigneeId;
        $this->status = $status;
    }

    /**
     * @return UuidInterface
     */
    public function getOrderId(): UuidInterface
    {
        return $this->orderId;
    }

    /**
     * @return UuidInterface|null
     */
    public function getAssigneeId(): ?UuidInterface
    {
        return $this->assigneeId;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function hasAssigneeId(): bool
    {
        return $this->assigneeId !== null;
    }
}