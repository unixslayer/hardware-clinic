<?php

namespace App\Domain;

class DeviceType
{
    const TYPE_DESKTOP = 'desktop';
    const TYPE_LAPTOP = 'laptop';
    const TYPE_MOBILE = 'mobile';
    const TYPE_TABLET = 'tablet';

    /**
     * @return string[]
     */
    public static function getDevices(): array
    {
        return [self::TYPE_DESKTOP, self::TYPE_LAPTOP, self::TYPE_MOBILE, self::TYPE_TABLET];
    }
}
