<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class OnAuthenticationSuccessRedirectProvider
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @param Security              $security
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(Security $security, UrlGeneratorInterface $urlGenerator)
    {
        $this->security = $security;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @return RedirectResponse
     */
    public function getRedirect(): RedirectResponse
    {
        if ($this->security->isGranted('ROLE_EMPLOYEE')) {
            return new RedirectResponse($this->urlGenerator->generate('employee_index'));
        }

        if ($this->security->isGranted('ROLE_CUSTOMER')) {
            return new RedirectResponse($this->urlGenerator->generate('customer_index'));
        }
    }
}