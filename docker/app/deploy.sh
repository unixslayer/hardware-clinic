#!/usr/bin/env bash

while [[ $(mysqladmin ping -h${DATABASE_HOST} -p${DATABASE_PASS} -u${DATABASE_USER} --silent) != *"is alive"* ]]; do
  echo "Waiting for database connection on host ${DATABASE_HOST}..."
  sleep 2
done

printf "\nRunning Doctrine migrations\n"
bin/console doctrine:migrations:migrate -n

printf "\nLoading clinic fixtures. Dictionaries, etc.\n"
bin/console doctrine:fixtures:load -n --group=clinic
