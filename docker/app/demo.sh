#!/usr/bin/env bash

printf "\n${blue}Loading demo fixtures${reset}\n"
bin/console doctrine:fixtures:load -n --append --group=demo
